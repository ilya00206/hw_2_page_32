#!/usr/bin/env python3
"""Task 1 from page 32"""


class Vehicle:
    """Vehicle abstraction class."""
    pass


class MotorVehicle(Vehicle):
    """Motor vehicle abstraction class."""
    pass


class Aircraft(Vehicle):
    """Aircraft abstraction class."""
    pass


class Watercraft(Vehicle):
    """Watercraft abstraction class."""
    pass


class Car(MotorVehicle):
    """Car abstraction class."""
    pass


class Truck(MotorVehicle):
    """Truck abstraction class."""
    pass


class Airplane(Aircraft):
    """Airplane abstraction class."""
    pass


class Helicopter(Aircraft):
    """Helicopter abstraction class."""
    pass


class Ship(Watercraft):
    """Ship abstraction class."""
    pass


class Boat(Watercraft):
    """Boat abstraction class."""
    pass
